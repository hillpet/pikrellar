#################################################################################
# 
# makefile:
#	$(TARGET) - Raspberry Pi PiKrellar motion archiving utility
#
#	Copyright (c) 2021 Peter Hillen
#################################################################################
BOARD=$(shell cat ../../RPIBOARD)

ifeq ($(BOARD),8)
#
# RPi#8 (Rev-2): Pond CAM
#
	BOARD_OK=yes
	TARGET=pikrellar
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="pikrellman.service"'
endif

ifeq ($(BOARD),10)
#
# RPi#10 (Rev-3): SpiCam HTTP Server
#
	BOARD_OK=yes
	TARGET=pikrellar
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="pikrellman.service"'
endif

ifeq ($(BOARD),12)
#
# RPi#12 (Rev-4): Carport CAM
#
	BOARD_OK=yes
	TARGET=pikrellar
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="pikrellman.service"'
endif

ifeq ($(BOARD),14)
#
# RPi#14 (Rev-4): Develop
#
	BOARD_OK=yes
	TARGET=pikrellar
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="pikrellman.service"'
endif

ifndef BOARD_OK
#
# All other RPi's: 
#
	BOARD_OK=yes
	TARGET=pikrellar
	COMMON=../common
	OBJX=
	DEFS=-D FEATURE_IO_NONE -D'THE_APP="pikrellman.service"'
endif

CC		= gcc
CFLAGS	= -O2 -Wall -g
DEFS	+= -D BOARD_RPI$(BOARD)
INCDIR	+= -I/opt/vc/include
INCDIR	+= -I/opt/vc/include/interface/vmcs_host/linux
INCDIR	+= -I/opt/vc/include/interface/vcos/pthreads -DRPI=1
INCDIR	+= -I$(COMMON)
LDFLGS	= -pthread
DESTDIR	= ./bin
LIBFLGS	= -L/usr/local/lib -L/usr/lib -L/opt/vc/lib -L$(COMMON)
LIBS	= -lrt
LIBCOM	= -lcommon
DEPS	= config.h
OBJ		= rpi_main.o
OBJ		+= $(OBJX)

debug: LIBCOM = -lcommondebug
debug: DEFS += -D FEATURE_USE_PRINTF -D DEBUG_ASSERT
debug: all

all: modules $(DESTDIR)/$(TARGET)

modules:
$(DESTDIR)/$(TARGET): $(OBJ)
	$(CC) $(CFLAGS) $(DEFS) -o $@ $(OBJ) $(LIBFLGS) $(LIBS) $(LIBCOM) $(LDFLGS)
	strip $(DESTDIR)/$(TARGET)

sync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp -u /mnt/rpi/softw/$(TARGET)/*.c ./
	cp -u /mnt/rpi/softw/$(TARGET)/*.h ./
	cp -u /mnt/rpi/softw/$(TARGET)/makefile ./
	cp -u /mnt/rpi/softw/$(TARGET)/rebuild ./
	cp -u /mnt/rpi/softw/$(TARGET)/pikrelltest ./

forcesync:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	cp /mnt/rpi/softw/$(TARGET)/*.c ./
	cp /mnt/rpi/softw/$(TARGET)/*.h ./
	cp /mnt/rpi/softw/$(TARGET)/makefile ./
	cp /mnt/rpi/softw/$(TARGET)/rebuild ./
	cp /mnt/rpi/softw/$(TARGET)/pikrelltest ./

clean:
	@echo "RPi-$(TARGET) Board=RPi#$(BOARD) $(DEFS)"
	rm -rf *.o
	rm -rf $(DESTDIR)/$(TARGET)

install:
	sudo cp bin/$(TARGET) /usr/bin/$(TARGET)

.c.o: $(DEPS)
	$(CC) -c $(CFLAGS) $(DEFS) $(INCDIR) $< 
