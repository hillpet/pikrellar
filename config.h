/*  (c) Copyright:  2018..2019  Patrn, Confidential Data
 *
 *  Workfile:           config.h
 *  Purpose:            Config file for pikrellar
 *
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Author:             Peter Hillen
 *  Changes:
 *    12 Jun 2021:      Created
 *
 *
 *
 * 
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _CONFIG_H_
#define _CONFIG_H_

//=============================================================================
// Feature switches
//
// makefile:
// If make debug:
//    -D DEBUG_ASSERT
//    -D FEATURE_USE_PRINTF (or overrule here)
//
//#define FEATURE_USE_PRINTF                 // Release mode: disable PRINTFs

//=============================================================================
//
// IO-Options are now defined in the makefile and the actual board itself:
// Rpi-Proj root: file RPIBOARD --> decimal number 1...?
//
// IO:   select define $(INOUT):
//       FEATURE_IO_NONE                  // IO : No IO used !
//       FEATURE_IO_PATRN                 // IO : Use Patrn.nl board
//

//=============================================================================
#ifdef   FEATURE_IO_NONE      // IO : NO IO board used
//=============================================================================
#define  RPI_IO_BOARD         "Pikrellar uses NO IO !"
//
#define  LED_R
#define  LED_Y
#define  LED_G
#define  LED_W
//
#define  REL_1                0
#define  REL_2                1
//
#define  BTN_1                0
#define  BTN_2                1
//
#define  INP_GPIO(x)        
#define  OUT_GPIO(x)     
#define  BUTTON(x)            FALSE
#define  LED(x, y)            
#define  OUT(x, y)
#endif

//=============================================================================
#ifdef   FEATURE_IO_PATRN     // IO : Use Patrn.nl board
//=============================================================================
#include <wiringPi.h>
//
#define  RPI_IO_BOARD         "Pikrellar uses PatrnBoard"
//
#define  LED_R                0
#define  LED_Y                7
#define  LED_G                3
#define  LED_W                6
//
#define  REL_1                11
#define  REL_2                10
//
#define  BTN_1                1
#define  BTN_2                4
//
#define  INP_GPIO(x)          pinMode(x, INPUT)
#define  OUT_GPIO(x)          pinMode(x, OUTPUT)
#define  BUTTON(x)            !digitalRead(x)
#define  LED(x, y)
#define  OUT(x, y)            digitalWrite(x, y)
#endif

#define DATA_VALID_SIGNATURE        0xDEADBEEF           // Valid signature
#define DATA_VERSION_MAJOR          1                    //
#define DATA_VERSION_MINOR          1                    //
#define MAX_NUM_DAYS                7                    // Keep 7 days of motion data on file
#define MAX_PATH_LEN                255                  //
#define MAX_PATH_LENZ               MAX_PATH_LEN+1       //
//
#define RPI_PUBLIC_WWW              "/all/public/www/"
#define RPI_PUBLIC_MOTION           "/mnt/rpicache/"
#define RPI_PUBLIC_PIX              "pix/"
#define RPI_PUBLIC_VIDEO            "video/"
#define RPI_PUBLIC_PIX_EXT          "hdd/pix/"
#define RPI_PUBLIC_VIDEO_EXT        "hdd/video/"
#define RPI_PUBLIC_DEFAULT          "index.html"
#define RPI_MOTION                  "motion_"
//
#define RPI_BASE_NAME               "pikrellar"
#define RPI_ERR_FILE                RPI_BASE_NAME ".err"
#define RPI_SHELL_FILE              RPI_BASE_NAME ".txt"
#define RPI_ALL_FILES               RPI_BASE_NAME ".*"
//
// Target RAM disk for high load file access
//
#define RPI_RUN_DIR                 "/run/pikrellcam/"
#define RPI_HOME_DIR                ""
#define RPI_WORK_DIR                "/mnt/rpicache/"
#define RPI_RAMFS_DIR               "/mnt/rpipix/"
#define RPI_BACKUP_DIR              "/usr/local/share/rpi/"
//
#define RPI_PIKRELL_LOG             RPI_WORK_DIR "Pikrellar.err"
#define RPI_PUBLIC_LOG_PATH         RPI_WORK_DIR RPI_LOG_FILE
#define RPI_ERROR_PATH              RPI_WORK_DIR RPI_ERR_FILE
#define RPI_MAP_PATH                RPI_WORK_DIR RPI_MAP_FILE
#define RPI_MAP_NEWPATH             RPI_WORK_DIR RPI_BASE_NAME "new.map"
#define RPI_PIKRELL_MOTION          RPI_PUBLIC_MOTION RPI_MOTION
//
//
// The build process determines RELEASE or DEBUG builds through the
// DEBUG_ASSERT compile-time switch.
//
#ifdef  DEBUG_ASSERT
//=============================================================================
// DEBUG
//=============================================================================
#define VERSION                     "1.00-CR001"
#define RPI_MAP_FILE                RPI_BASE_NAME "_debug.map"
#define RPI_LOG_FILE                RPI_BASE_NAME "_debug.log"
//
#define RPI_PIKRELL_STATE           RPI_HOME_DIR "state"
#define RPI_PIKRELL_EVENTS          RPI_HOME_DIR "motion-events"
#define RPI_PIKRELL_ARGUMENTS       RPI_RUN_DIR "motion-args"

#else   //DEBUG_ASSERT
//=============================================================================
// RELEASE
//=============================================================================
#define VERSION                     "1.00.001"
#define RPI_MAP_FILE                RPI_WORK_DIR RPI_BASE_NAME ".map"
#define RPI_LOG_FILE                RPI_WORK_DIR RPI_BASE_NAME ".log"
//
#define RPI_PIKRELL_STATE           RPI_RUN_DIR "state"
#define RPI_PIKRELL_EVENTS          RPI_RUN_DIR "motion-events"
#define RPI_PIKRELL_ARGUMENTS       RPI_RUN_DIR "motion-args"

#endif  //DEBUG_ASSERT

#endif /* _CONFIG_H_ */

