/*  (c) Copyright:  2021  Patrn, Confidential Data
 *
 *  Workfile:           rpi_main.h
 *  Purpose:            Headerfile for main thread
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    12 Jun 2021:      Created
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#ifndef _RPI_MAIN_H_
#define _RPI_MAIN_H_

#define PID_PIKRELLMAN  "pikrellman"

typedef struct _arg_list_
{
   const char *pcArg;
   const char *pcDefault;
   const char *pcHelp;
}  ARGL;

#endif /* _RPI_MAIN_H_ */
