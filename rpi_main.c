/*  (c) Copyright:  2021 Patrn, Confidential Data
 *
 *  Workfile:           rpi_main.c
 *  Purpose:            This utility is executed by the on_motion_end $C/motion-archive-rpi.sh script
 *                      and triggers the loop_video file archiving.
 *  Compiler/Assembler: Raspbian Linux GNU gcc
 *  Ext Packages:       PiKrellCam motion detect
 *  Note:               
 *
 *  Author:             Peter Hillen
 *  Changes:
 *    12 Jun 2021:      Created
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include <stdio.h>
#include <string.h>
#include <semaphore.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <resolv.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <errno.h>

#include <common.h>
#include "config.h"
//
#include "rpi_main.h"
//
#define USE_PRINTF
#include "printf.h"

//
// Define the order in which the arguments file is composed:
//
static ARGL stArgList[] =
{
   //  pcArg                pcDefault                                    pcHelp
   {  "caller",            "",                                          "   Caller                   "    },
   {  "logfile",           "/mnt/rpicache/pikrellar.log",               "$G LOG file                 "    },
   {  "fifo",              "/home/pi/pikrellcam/FIFO",                  "$P FIFO Full Path           "    }, 
   {  "last_video",        "",                                          "$v Last Saved Path Video    "    }, 
   {  "last_thumb",        "",                                          "$A Last Saved Path Thumbnail"    },
   {  "media_dir",         "/all/public/www/media",                     "$m Media Dir                "    },
   {  "archive_dir",       "/all/public/www/media/archive",             "$a Archive Dir              "    },
   {  "video_dir",         "/all/public/www/media/videos",              "$V Video Dir                "    },
   {  "thumb_dir",         "/all/public/www/media/thumbs",              "$t Thumb Dir                "    },
   {  "loop_dir",          "/mnt/rpipix/videos",                        "$z Loop recording Dir       "    },
   {  "still_dir",         "/all/public/www/media/stills",              "$S Still Dir                "    },
   {  "timelapse_dir",     "/all/public/www/media/timelapse",           "$L Timelapse Dir            "    },
   {  "flags",             "0",                                         "   Flags and Verbose level  "    }
};
static int iNumArgList = sizeof(stArgList)/sizeof(ARGL);
//
// Local data/ functions
//
static int        rpi_StartService        (const char *);
static int        rpi_WriteArgumentFile   (int, char **);
//
static GLOG        G_stLog;
static const char *pcPiKrellMan  = PID_PIKRELLMAN;
static const char *pcArgsFile    = RPI_PIKRELL_ARGUMENTS;
static const char *pcArgsFileTmp = RPI_PIKRELL_ARGUMENTS ".tmp";
//
// Common lib needs support if no globals.*:
//
int   GLOBAL_GetTraceCode()            {return(0);}
int   GLOBAL_GetDebugMask()            {return(0);}
int   GLOBAL_GetMallocs()              {return(0);}
void  GLOBAL_PutMallocs(int i)         {}
GLOG *GLOBAL_GetLog()                  {return(&G_stLog);}
bool  GLOBAL_CheckDelete(char *pcPath) {return(FALSE);}

/* ======   Local Functions separator ===========================================
___MAIN_FUNCTIONS(){}
==============================================================================*/

//
// Function:   main
// Purpose:    Main entry point from PiKrellCam
//             on_motion_end $C/motion-archive-rpi.sh script
//
// Parms:      Commandline options
// Returns:    Exit codes
// Note:       The motion-archive-rpi.sh script calls pikrellar to run at the end
//             of motion-detection (if this script is configured to be run as the 
//             on_motion_end command). 
//
//             "on_motion_end $C/motion-detect-rpi.sh "$@"
//             motion-archive-rpi.sh will call /usr/bin/pikrellar "$@"
//
//             Motion can be determined further by examining 
//             "/run/pikrellcam/motion-events".
//             This script should be configured in ~/.pikrellcam/pikrellcam.conf and not
//             in the at-command.conf file.  Eg in pikrellcam.conf:
//             on_motion_end $C/motion-archive-rpi.sh $G $P $v $A $m $a $V $t $z $S $L 768"
//             Argument substitution done by PiKrellCam before running this script:
//             
//               $C - scripts directory so this script is found.
//               $G - log file configured in ~/.pikrellcam/pikrellcam.conf.
//               $P - the command FIFO so we can write commands to PiKrellCam.
//               $v - the full path name of the motion video just saved.
//               $A - the full path name of the last motion thumbnail file.
//               $m - the full path name of the Media     directory
//               $a - the full path name of the Archive   directory
//               $V - the full path name of the Videos    directory
//               $t - the full path name of the Thumbs    directory
//               $z - the full path name of the Loop      directory
//               $S - the full path name of the Still     directory
//               $L - the full path name of the Timelapse directory
//               xx - Flags and verbose level
//
int main(int argc, char **argv)
{
   int   iCc;
   pid_t tPid;

   PRINTF("rpi-main():PiKrellAr running..." CRLF);
   //
   // Pass all necessary arguments to its arguments file.
   //
   if( (iCc = rpi_WriteArgumentFile(argc, argv)) == EXIT_CC_OKEE)
   {
      tPid = GEN_GetPidByName(pcPiKrellMan);
      if(tPid == 0) 
      {
         //
         // THE_APP is not yet running: Start the service 
         //
         PRINTF1("rpi-main():Pidof %s not yet running:Start service" CRLF, pcPiKrellMan);
         if( (iCc  = rpi_StartService(pcPiKrellMan)) == EXIT_CC_OKEE)
         {
            GEN_Sleep(100);
            tPid = GEN_GetPidByName(pcPiKrellMan);
         }
      }
      if(tPid > 0) 
      {
         //
         // Signal the PiKrekkMan to take action by:
         //    - We have updated the arguments file
         //    - sending SIGUSR2
         //
         PRINTF2("rpi-main():Signal %s on PID=%d" CRLF, pcPiKrellMan, tPid);
         kill(tPid, SIGUSR2);
         PRINTF("rpi-main():PiKrellAr EXIT OKee" CRLF);
      }
      else
      {
         PRINTF("rpi-main():PiKrellAr EXIT ERROR" CRLF);
      }
   }
   else
   {
      PRINTF("rpi-main():PiKrellAr EXIT ERROR" CRLF);
   }
   return(iCc);
}

/*------  Local functions separator ------------------------------------
__LOCAL_FUNCTIONS_________(){};
----------------------------------------------------------------------------*/

// 
// Function:   rpi_StartService
// Purpose:    Start the PiKrellMan service and store the necessary arguments in 
//             the arguments file RPI_PIKRELL_ARGUMENTS.
// 
// Parms:      Service name
// Returns:    See below
// Note:       The system() library function uses fork(2) to create a child process
//             that executes the shell command specified in command using execl(3)
//             as follows: execl("/bin/sh", "sh", "-c", command, (char *) NULL);
//             system() returns after the command has been completed.
//     
//             During execution of the command, SIGCHLD will be blocked, and SIGINT
//             and SIGQUIT will be ignored, in the process that calls system().
//             (These signals will be handled according to their defaults inside the
//             child process that executes command.)
//     
//             The return value of system() is one of the following:
//             *  If command is NULL, then a nonzero value if a shell is available,
//                  or 0 if no shell is available.
//     
//             *  If a child process could not be created, or its status could not
//                  be retrieved, the return value is -1 and errno is set to indicate
//                  the error.
//     
//             *  If a shell could not be executed in the child process, then the
//                  return value is as though the child shell terminated by calling
//                  _exit(2) with the status 127.
//     
//             *  If all system calls succeed, then the return value is the
//                  termination status of the child shell used to execute command.
//                  (The termination status of a shell is the termination status of
//                  the last command it executes.)
//     
//             In the last two cases, the return value is a "wait status" that can
//             be examined using the macros described in waitpid(2).  (i.e.,
//             WIFEXITED(), WEXITSTATUS(), and so on).
// 
static int rpi_StartService(const char *pcService)
{
   int      iCc;
   char    *pcShell;

   pcShell = safemalloc(MAX_PATH_LENZ);
   //
   GEN_SNPRINTF(pcShell, MAX_PATH_LEN, "sudo systemctl start %s.service", pcService);
   PRINTF1("rpi-StartService(): Exec system[%s]" CRLF, pcShell);
   iCc = system(pcShell);
   if(iCc < 0)
   {
      // error completion
      PRINTF1("rpi-StartService(): Exec completion: cc=%d" CRLF, iCc);
      LOG_Report(errno, "RPI", "rpi-StartService(): ERROR starting %s", pcShell);
      GEN_ReportProcessStatus(iCc);
   }
   else iCc = WEXITSTATUS(iCc);
   safefree(pcShell);
   return(iCc);
}

// 
// Function:   rpi_WriteArgumentFile
// Purpose:    Write a new arguments file
// 
// Parms:      Number of args, args ptr
// Returns:    Completion code 
// Not:        
//             
static int rpi_WriteArgumentFile(int iArgc, char **ppcArgs)
{
   int   iCc=EXIT_CC_GEN_ERROR;
	FILE *ptFile;
   int   i; 

	ptFile = safefopen((char *)pcArgsFileTmp, "w");
   if(ptFile)
   {
      for(i=0; i<iNumArgList; i++)
      {
         if(i < iArgc)
         {
	         GEN_FPRINTF(ptFile, "%s %s\n", stArgList[i].pcArg, ppcArgs[i]);
	         PRINTF2("rpi-WriteArgumentFile():Argument [%s] = [%s]" CRLF, stArgList[i].pcHelp, ppcArgs[i]);
         }
         else
         {
	         GEN_FPRINTF(ptFile, "%s %s\n", stArgList[i].pcArg, stArgList[i].pcDefault);
	         PRINTF3("rpi-WriteArgumentFile():Argument [%s=%s] not supplied on command-line, use default [%s]!" CRLF, stArgList[i].pcArg, stArgList[i].pcHelp, stArgList[i].pcDefault);
         }
      }
	   safefclose(ptFile);
      //
      // Replace the previous arguments file
      //
	   iCc = rename(pcArgsFileTmp, pcArgsFile);
   }
   return(iCc);
}
